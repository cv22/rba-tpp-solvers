// rubber_band_algo.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#//include "pch.h"
#include <iostream>
#include <cmath>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <chrono>
#include <stdlib.h>


using namespace std;


double FLOAT_EQ_CT = 0.00001;
int SEED = 1;
int NR_TESTS = 2500;
int MIN_RADIUS = 5;
int MAX_RADIUS = 5;
int MIN_POINTS = 10;
int MAX_POINTS = 10;
int MAX_X = 100;
int MAX_Y = 100;
int MAX_NR_ITERATIONS = 10000;


struct point_2d
{
	int index;
	double x;
	double y;
	double radius;
	double x_circuit;
	double y_circuit;

	void set_point_2d(int arg_index, double arg_x, double arg_y, double arg_radius,double arg_x_circuit, double arg_y_circuit)
	{
		index = arg_index;
		x = arg_x;
		y = arg_y;
		radius = arg_radius;
		x_circuit = arg_x_circuit;
		y_circuit = arg_y_circuit;
	}

	void copy(point_2d data_in)
	{
		index = data_in.index;
		x = data_in.x;
		y = data_in.y;
		radius = data_in.radius;
		x_circuit = data_in.x_circuit;
		y_circuit = data_in.y_circuit;
	}


	void print()
	{
		cout << index << " " <<x << " " << y << " radius= "<< radius<< " circuit xy= "<< x_circuit << " " << y_circuit <<  endl;
	}

	string to_string()
	{
		return std::to_string((long long)index) + " " + std::to_string((long double)x) + 
			" " + std::to_string((long double)y) + " radius= " + std::to_string((long double)radius)
			+ " circuit xy= " + std::to_string((long double)x_circuit) + " " + std::to_string((long double)y_circuit) + "\n";
	}

	int check_equals_xy_circuit(point_2d arg_in)
	{
		if ((x_circuit == arg_in.x_circuit) && (y_circuit == arg_in.y_circuit))
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}


};


double calculate_distance(point_2d a, point_2d b)
{
	return sqrt((a.x_circuit - b.x_circuit)*(a.x_circuit - b.x_circuit) + (a.y_circuit - b.y_circuit)*(a.y_circuit - b.y_circuit));
}


double calculate_distance_centers(point_2d a, point_2d b)
{
	return sqrt((a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y));
}



double calculate_total_distance(vector <point_2d>  circuit)
{
	double result = 0;
	double distance = 0;
	for (unsigned int i = 0; i < circuit.size() - 1; i++)
	{
		distance = calculate_distance(circuit[i], circuit[i+1]);
		result = result + distance;
	}
	result = result + calculate_distance(circuit[circuit.size()-1], circuit[0]);
	return result;
}


void print_circuit(vector <point_2d>  circuit)
{
	cout << "total circuit length = " << calculate_total_distance(circuit) << endl;
	for (unsigned int i = 0; i < circuit.size(); i++)
	{
		circuit[i].print();
	}
	cout << endl << endl << endl << endl << endl;
}

string circuit_to_string(vector <point_2d>  circuit)
{
	string result = "";
	result = result + "total circuit length = " + std::to_string((long double)calculate_total_distance(circuit)) + "\n";
	for (unsigned int i = 0; i < circuit.size(); i++)
	{
		result = result + circuit[i].to_string();
	}
	result = result + "\n\n\n";
	return result;
}



point_2d calculate_perpendicular_point(point_2d point_on_line_a, point_2d point_on_line_b, point_2d third_point)
{
	// a1 x + b1 y + c = 0
	// y = m1 x + m1c;
	point_2d result;
	//calculate coefficients
	double a1, b1, c1, m1, m1c;
	double m2, m2c;
	a1 = point_on_line_a.y_circuit - point_on_line_b.y_circuit;
	b1 = point_on_line_a.x_circuit - point_on_line_b.x_circuit;
	c1 = (point_on_line_a.x_circuit * point_on_line_b.y_circuit) - (point_on_line_b.x_circuit * point_on_line_a.y_circuit);
	m1 = a1 / b1;
	m1c = c1 / b1;

	//perpendicular line
	m2 = -1 / m1;
	m2c = third_point.y_circuit - m2 * third_point.x_circuit;


	//special case for third point on the same line, perpendicular undefined, returns third point itself
	if (abs(m1*third_point.x_circuit + m1c - third_point.y_circuit) < FLOAT_EQ_CT)
	{
		//cout << "error. third point on the same line, perpendicular undefined.";
		return third_point;
	}

	//calculate point; special situations for horizontal/vertical line
	if (abs(point_on_line_a.x_circuit - point_on_line_b.x_circuit) < FLOAT_EQ_CT)
	{
		result.x_circuit = point_on_line_a.x_circuit;
		result.y_circuit = third_point.y_circuit;
		result.x = result.x_circuit;
		result.y = result.y_circuit;
		return result;
	}
	else
	{
		result.x_circuit = ((m2c - m1c) / (m1 - m2));
		result.x = result.x_circuit;
	}

	if (abs(point_on_line_a.y_circuit - point_on_line_b.y_circuit) < FLOAT_EQ_CT)
	{
		result.y_circuit = point_on_line_a.y_circuit;
		result.x_circuit = third_point.x_circuit;
		result.y = result.y_circuit;
		result.x = result.x_circuit;
		return result;
	}
	else
	{
		result.y_circuit = (m1 * result.x_circuit + m1c);
		result.y = result.y_circuit;
	}


	return result;
}



int check_point_within_circle(point_2d point, point_2d circle)
{
	return (	(point.x_circuit - circle.x)*(point.x_circuit - circle.x) +
				(point.y_circuit - circle.y)*(point.y_circuit - circle.y) < 
				circle.radius*circle.radius
			);
}




//i want distance to segment, not to infinite 2d line
double calculate_distance_point_segment(point_2d point_on_line_a, point_2d point_on_line_b, point_2d third_point)
{
	if (calculate_distance(point_on_line_a, point_on_line_b) < FLOAT_EQ_CT) // (line==0) basically => points overlap
	{
		return calculate_distance(point_on_line_a, point_on_line_b);
	}

	point_2d perpendicular_point;
	perpendicular_point = calculate_perpendicular_point(point_on_line_a, point_on_line_b, third_point);


	double min_x, max_x;
	double min_y, max_y;

	min_x = min(point_on_line_a.x_circuit, point_on_line_b.x_circuit);
	max_x = max(point_on_line_a.x_circuit, point_on_line_b.x_circuit);

	min_y = min(point_on_line_a.y_circuit, point_on_line_b.y_circuit);
	max_y = max(point_on_line_a.y_circuit, point_on_line_b.y_circuit);


	if (
		(min_x <= perpendicular_point.x_circuit) && (perpendicular_point.x_circuit <= max_x) &&
		(min_y <= perpendicular_point.y_circuit) && (perpendicular_point.y_circuit <= max_y)
		)
	{
		//return calculate_distance_point_line(point_on_line_a,point_on_line_b,third_point);
		return calculate_distance(third_point, perpendicular_point);
	}
	else
	{
		return min(calculate_distance(point_on_line_a, third_point), calculate_distance(point_on_line_b, third_point));
	}


}





double calculate_distance_point_center_segment(point_2d point_on_line_a, point_2d point_on_line_b, point_2d third_point)
{
	if (calculate_distance(point_on_line_a, point_on_line_b) < FLOAT_EQ_CT) // (line==0) basically => points overlap
	{
		return calculate_distance(point_on_line_a, point_on_line_b);
	}

	point_2d perpendicular_point;
	third_point.x_circuit = third_point.x;
	third_point.y_circuit = third_point.y;
	perpendicular_point = calculate_perpendicular_point(point_on_line_a, point_on_line_b, third_point);


	double min_x, max_x;
	double min_y, max_y;

	min_x = min(point_on_line_a.x_circuit, point_on_line_b.x_circuit);
	max_x = max(point_on_line_a.x_circuit, point_on_line_b.x_circuit);

	min_y = min(point_on_line_a.y_circuit, point_on_line_b.y_circuit);
	max_y = max(point_on_line_a.y_circuit, point_on_line_b.y_circuit);


	if (
		(min_x <= perpendicular_point.x_circuit) && (perpendicular_point.x_circuit <= max_x) &&
		(min_y <= perpendicular_point.y_circuit) && (perpendicular_point.y_circuit <= max_y)
		)
	{
		//return calculate_distance_point_line(point_on_line_a,point_on_line_b,third_point);
		return calculate_distance(third_point, perpendicular_point);
	}
	else
	{
		return min(calculate_distance(point_on_line_a, third_point), calculate_distance(point_on_line_b, third_point));
	}


}




/// optimizez position of middle point. geometric implementation
/// returns optimized position
/// XXXX todo momentan are cercul centrat in 0,0 . not sure cum sa il mut
/// XXXX NOT WORKING; also faza cu x si x_circuit
/*
point_2d optimize_points_rubber_band(point_2d left_point, point_2d right_point, point_2d middle_point)
{
	point_2d result;
	result.copy(middle_point);

	point_2d perpendicular_point;
	perpendicular_point.copy(calculate_perpendicular_point(left_point, right_point, middle_point));


	double a, b, c, m1, m1c, r;
	
	if (check_point_within_circle(perpendicular_point,middle_point))
	{ //fac intersectia dintre segmentul original si cerc
		a = left_point.y - right_point.y;
		b = left_point.x - right_point.x;
		c = (left_point.x * right_point.y) - (right_point.x * left_point.y);
	}
	else
	{ //fac intersectia dintre perpendiculara si cerc
		a = middle_point.y - perpendicular_point.y;
		b = middle_point.x - perpendicular_point.x;
		c = (middle_point.x * perpendicular_point.y) - (perpendicular_point.x * middle_point.y);
	}
	m1 = a / b;
	m1c = c / b;
	r = middle_point.radius;
	

	double x0 = -a * c / (a*a + b * b);
	double y0 = -b * c / (a*a + b * b);
	if (c*c - r * r*(a*a + b * b)  > FLOAT_EQ_CT)
	{
		cout << "no points";
	}
	else
	{
		if (abs(c*c - r * r*(a*a + b * b)) < FLOAT_EQ_CT)
		{
			//cout << "1 point" << x0 << ' ' << y0 << '\n';
			result.set_point_2d(middle_point.index, x0, y0, middle_point.radius,);
		}
		else //in my case, should always go here
		{
			double d = r * r - c * c / (a*a + b * b);
			double mult = sqrt(d / (a*a + b * b));
			double ax, ay, bx, by;
			ax = x0 + b * mult;
			bx = x0 - b * mult;
			ay = y0 - a * mult;
			by = y0 + a * mult;
			
			point_2d temp1;
			point_2d temp2;
			temp1.set_point_2d(middle_point.index, ax, ay, middle_point.radius);
			temp2.set_point_2d(middle_point.index, bx, by, middle_point.radius);
			if (calculate_distance(temp1, perpendicular_point) > calculate_distance(temp2, perpendicular_point))
			{
				result.copy(temp2);
			}
			else
			{
				result.copy(temp1);
			}

		}
	}

	return result;
}//end point_2d optimize_points_rubber_band(point_2d left_point, point_2d right_point, point_2d middle_point)
//*/






/// optimizez position of middle point. geometric implementation
// probably wrong
/// returns optimized position
/*
point_2d optimize_points_rubber_band_2(point_2d left_point, point_2d right_point, point_2d middle_point)
{
	point_2d result;
	result.copy(middle_point);

	point_2d perpendicular_point;
	perpendicular_point.copy(calculate_perpendicular_point(left_point, right_point, middle_point));


	double a, b, c, m1, m1c, r;

	if (check_point_within_circle(perpendicular_point, middle_point))
	{ //fac intersectia dintre segmentul original si cerc
		a = right_point.y_circuit - left_point.y_circuit ;
		b = right_point.x_circuit - left_point.x_circuit ;
		c = (right_point.x_circuit * left_point.y_circuit) - (left_point.x_circuit * right_point.y_circuit) ;
		m1 = a / b;
		m1c = c / b;
	}
	else
	{ //fac intersectia dintre perpendiculara si cerc
		a = perpendicular_point.y_circuit - middle_point.y_circuit ;
		b = perpendicular_point.x_circuit - middle_point.x_circuit;
		c = (perpendicular_point.x_circuit * middle_point.y_circuit) - (middle_point.x_circuit * perpendicular_point.y_circuit);
		m1 = a / b;
		m1c = c / b;
		m1 = -1 / m1;
		m1c = middle_point.y_circuit - m1 * middle_point.x_circuit;
	}
	
	r = middle_point.radius;

	// d*x^2 + e*x + f = 0 ; dupa inlocuire din circle equation a line equation

	double d = 1 + m1*m1;
	double e = -2 * middle_point.x + 2 * m1 * m1c - 2 * m1 * middle_point.y;
	double f = middle_point.x * middle_point.x + m1c * m1c - 2 * m1c * middle_point.y + middle_point.y*middle_point.y - r*r;

	if (d == 0)
	{
		if (e == 0)
		{
			if (f == 0)
			{
				cout << "optimize_points_rubber_band_2 error 2";
			}
			else
			{
				cout << "optimize_points_rubber_band_2 error 3";
			}
		}
		else
		{//o solutie
			point_2d temp1;
			temp1.x_circuit = -f / e;
			temp1.y_circuit = temp1.x_circuit*m1 + m1c;
			temp1.x = middle_point.x;
			temp1.y = middle_point.y;
			result.copy(temp1);
		}
	}
	else
	{
		double delta = e * e- 4 * d*f;
		if (delta < 0)
		{
			//0 solutii
			cout << "optimize_points_rubber_band_2 error 1";
		}
		else
		{
			if (delta <= FLOAT_EQ_CT)
			{//1 solutii
				point_2d temp1;
				temp1.x_circuit = -e / (2 * d);
				temp1.y_circuit = temp1.x_circuit*m1 + m1c;
				temp1.x = middle_point.x;
				temp1.y = middle_point.y;
				result.copy(temp1);
			}
			else
			{//2 solutii
				double x1 = (-e + sqrt(delta)) / (2 * d);
				double x2 = (-e - sqrt(delta)) / (2 * d);
				point_2d temp1;
				temp1.x_circuit = x1;
				temp1.y_circuit = temp1.x_circuit*m1 + m1c;
				point_2d temp2;
				temp2.x_circuit = x2;
				temp2.y_circuit = temp2.x_circuit*m1 + m1c;
				temp1.x = middle_point.x;
				temp1.y = middle_point.y;
				temp2.x = middle_point.x;
				temp2.y = middle_point.y;
				if (calculate_distance(temp1, perpendicular_point) > calculate_distance(temp2, perpendicular_point))
				{
					result.copy(temp2);
				}
				else
				{
					result.copy(temp1);
				}
			}
		}
	}

	result.index = middle_point.index;
	result.radius = middle_point.radius;
	return result;
}//end point_2d optimize_points_rubber_band_2(point_2d left_point, point_2d right_point, point_2d middle_point)
//*/




pair<point_2d,point_2d> find_point_segment_intersect_circle(point_2d point_on_line_a, point_2d point_on_line_b, point_2d circle)
{
	//gasesc cele 2 solutii pentru cerc middle point intersectat cu segmentul [left_point,middle_point]

	point_2d result1;
	result1.copy(circle);
	point_2d result2;
	result2.copy(circle);

	double a, b, c, m1, m1c;
	a = point_on_line_b.y_circuit - point_on_line_a.y_circuit;
	b = point_on_line_b.x_circuit - point_on_line_a.x_circuit;
	c = (point_on_line_b.x_circuit * point_on_line_a.y_circuit) - (point_on_line_a.x_circuit * point_on_line_b.y_circuit);
	m1 = a / b;
	m1c = c / b;
	// d*x^2 + e*x + f = 0 ; dupa inlocuire din circle equation a line equation
	double d = 1 + m1 * m1;
	double e = -2 * circle.x + 2 * m1 * m1c - 2 * m1 * circle.y;
	double f = circle.x * circle.x + m1c * m1c - 2 * m1c * circle.y + circle.y*circle.y - circle.radius * circle.radius;
	if (d == 0)
	{
		if (e == 0)
		{
			if (f == 0)
			{
				cout << "optimize_points_rubber_band_2 error 2";
			}
			else
			{
				cout << "optimize_points_rubber_band_2 error 3";
			}
		}
		else
		{//o solutie
			point_2d temp1;
			temp1.x_circuit = -f / e;
			temp1.y_circuit = temp1.x_circuit*m1 + m1c;
			temp1.x = circle.x;
			temp1.y = circle.y;
			result1.copy(temp1);
		}
	}
	else
	{
		double delta = e * e - 4 * d*f;
		if (delta < 0)
		{
			//0 solutii
			cout << "optimize_points_rubber_band_2 error 1";
		}
		else
		{
			if (delta <= FLOAT_EQ_CT)
			{//1 solutii
				point_2d temp1;
				temp1.x_circuit = -e / (2 * d);
				temp1.y_circuit = temp1.x_circuit*m1 + m1c;
				temp1.x = circle.x;
				temp1.y = circle.y;
				result1.copy(temp1);
			}
			else
			{//2 solutii
				double x1 = (-e + sqrt(delta)) / (2 * d);
				double x2 = (-e - sqrt(delta)) / (2 * d);
				point_2d temp1;
				temp1.x_circuit = x1;
				temp1.y_circuit = temp1.x_circuit*m1 + m1c;
				point_2d temp2;
				temp2.x_circuit = x2;
				temp2.y_circuit = temp2.x_circuit*m1 + m1c;
				temp1.x = circle.x;
				temp1.y = circle.y;
				temp2.x = circle.x;
				temp2.y = circle.y;
				result1.copy(temp1);
				result2.copy(temp2);
			}
		}
	}

	pair<point_2d, point_2d> pair_result;
	pair_result.first = result1;
	pair_result.second = result2;
	return pair_result;
}



pair <double, double> solve_second_order_equation(double a, double b, double c)
{
	pair<double,double> solution;
	if (a == 0)
	{
		if (b == 0)
		{
			if (c == 0)
			{
				cout << "solve_second_order_equation error 2";
			}
			else
			{
				cout << "solve_second_order_equation error 3";
			}
		}
		else
		{//o solutie
			solution.first = -c / b;
			solution.second = solution.first;
		}
	}
	else
	{
		double delta = b * b - 4 * a*c;
		if (delta < 0)
		{
			//0 solutii
			cout << "solve_second_order_equation error 1";
		}
		else
		{
			if (delta <= FLOAT_EQ_CT)
			{//1 solutii
				solution.first = -b / (2 * a);
				solution.second = solution.first;
			}
			else
			{//2 solutii
				solution.first = (-b + sqrt(delta)) / (2 * a);
				solution.second = (-b - sqrt(delta)) / (2 * a);
			}
		}
	}
	return solution;
}



//varianta cu pasi multi si aleg best position din miile testate. merge dar ffffff ineficient - brute force
//*
point_2d optimize_points_rubber_band_4(point_2d left_point, point_2d right_point, point_2d middle_point)
{
	//parcurg tot cercul. pt orice x gasesc cei 2 y. 

	double min_x = middle_point.x - middle_point.radius;
	double max_x = middle_point.x + middle_point.radius;
	point_2d point_to_test;
	double best_distance = 99999999;
	double temp_distance = 99999999;
	point_2d best_point;
	best_point.copy(middle_point);

	for (double x = min_x; x <= max_x; x += FLOAT_EQ_CT)
	{
		point_to_test.x_circuit = x;
		//pt orice x, caut cei 2 y de pe cerc.
		double a, b, c;
		a = 1;
		b = -2 * middle_point.y;	
		c = x - middle_point.x;
		c = c * c - middle_point.radius * middle_point.radius + middle_point.y*middle_point.y;

		if (a == 0)
		{
			if (b == 0)
			{
				if (c == 0)
				{
					cout << "optimize_points_rubber_band_2 error 2";
				}
				else
				{
					cout << "optimize_points_rubber_band_2 error 3";
				}
			}
			else
			{//o solutie
				point_to_test.y_circuit = -c / b;
				temp_distance = calculate_distance(left_point, point_to_test) + calculate_distance(right_point, point_to_test);
				if (temp_distance < best_distance)
				{
					best_point.x_circuit = x;
					best_point.y_circuit = point_to_test.y_circuit;
					best_distance = temp_distance;
				}
			}
		}
		else
		{
			double delta = b * b - 4 * a*c;
			if (delta < 0)
			{
				//0 solutii
				cout << "optimize_points_rubber_band_2 error 1";
			}
			else
			{
				if (delta <= FLOAT_EQ_CT)
				{//1 solutii
					point_to_test.y_circuit = -b / (2 * a);
					temp_distance = calculate_distance(left_point, point_to_test) + calculate_distance(right_point, point_to_test);
					if (temp_distance < best_distance)
					{
						best_point.x_circuit = x;
						best_point.y_circuit = point_to_test.y_circuit;
						best_distance = temp_distance;
					}
				}
				else
				{//2 solutii
					point_to_test.y_circuit = (-b + sqrt(delta)) / (2 * a);
					temp_distance = calculate_distance(left_point, point_to_test) + calculate_distance(right_point, point_to_test);
					if (temp_distance < best_distance)
					{
						best_point.x_circuit = x;
						best_point.y_circuit = point_to_test.y_circuit;
						best_distance = temp_distance;
					}
					point_to_test.y_circuit = (-b - sqrt(delta)) / (2 * a);
					temp_distance = calculate_distance(left_point, point_to_test) + calculate_distance(right_point, point_to_test);
					if (temp_distance < best_distance)
					{
						best_point.x_circuit = x;
						best_point.y_circuit = point_to_test.y_circuit;
						best_distance = temp_distance;
					}
				}
			}
		}


	}

	return best_point;
}//end point_2d optimize_points_rubber_band_4(point_2d left_point, point_2d right_point, point_2d middle_point)
//*/


int sgn(double arg_in)
{
	if (arg_in < 0)
	{
		return -1;
	}
	else
	{
		return 1;
	}
}



point_2d calculate_line_line_intersection(point_2d line0_point0, point_2d line0_point1, point_2d line1_point0, point_2d line1_point1)
{
	point_2d result;
	double a1, b1, c1,m1,m1c;
	double a2, b2, c2,m2,m2c;

	a1 = line0_point0.y_circuit - line0_point1.y_circuit;
	b1 = line0_point1.x_circuit - line0_point0.x_circuit;
	c1 = (line0_point0.x_circuit * line0_point1.y_circuit) - (line0_point1.x_circuit * line0_point0.y_circuit);
	if (b1 == 0)
	{
		b1 = b1 + FLOAT_EQ_CT;
	}
	m1 = -a1 / b1;
	m1c = -c1 / b1;

	a2 = line1_point0.y - line1_point1.y;
	b2 = line1_point1.x - line1_point0.x;
	c2 = (line1_point0.x * line1_point1.y) - (line1_point1.x * line1_point0.y);
	if (b2 == 0)
	{
		b2 = b2 + FLOAT_EQ_CT;
	}
	m2 = -a2 / b2;
	m2c = -c2 / b2;

	//check if same line or parallel
	if ((m1 == m2 + FLOAT_EQ_CT) || (m1 == m2 - FLOAT_EQ_CT))
	{
		cout << "ERROR: calculate_line_line_intersection between 2 parallel lines";
	}

	// egalez y si scot x. 
	result.x = (m2c-m1c) / (m1-m2);
	result.x_circuit = result.x;
	// bag orice x si iese un  y 
	result.y = -(a1*result.x+c1)/b1;
	result.y_circuit = result.y;

	return result;
}


pair< point_2d, point_2d> calculate_circle_line_intersection(point_2d point_on_line_a, point_2d point_on_line_b, point_2d circle_center)
{
	point_2d solution0;
	point_2d solution1;

	solution0.copy(circle_center);
	solution1.copy(circle_center);

	//from http://mathworld.wolfram.com/Circle-LineIntersection.html
	//place circle origin in (0,0); also move other points
	double x_recentering_movement = circle_center.x;
	double y_recentering_movement = circle_center.y;
	circle_center.x = 0;
	circle_center.y = 0;
	point_on_line_a.x_circuit = point_on_line_a.x_circuit - x_recentering_movement;
	point_on_line_b.x_circuit = point_on_line_b.x_circuit - x_recentering_movement;
	point_on_line_a.y_circuit = point_on_line_a.y_circuit - y_recentering_movement;
	point_on_line_b.y_circuit = point_on_line_b.y_circuit - y_recentering_movement;

	double dx = point_on_line_b.x_circuit - point_on_line_a.x_circuit;
	double dy = point_on_line_b.y_circuit - point_on_line_a.y_circuit;
	double dr = sqrt(dx*dx + dy*dy);
	double det = point_on_line_a.x_circuit*point_on_line_b.y_circuit - point_on_line_b.x_circuit*point_on_line_a.y_circuit;

	double delta = circle_center.radius * circle_center.radius * dr*dr - det*det;

	if (delta < -FLOAT_EQ_CT)// delta < 0
	{
		cout << "ERROR : calculate_circle_line_intersection error 0, no intersection";
	}
	else
	{
		solution0.x_circuit = (det * dy + sgn(dy)*dx*sqrt(delta))/(dr*dr);
		solution0.y_circuit = (-det * dx + abs(dy)*sqrt(delta)) / (dr*dr);
		if ((delta > -FLOAT_EQ_CT)&&((delta < FLOAT_EQ_CT)) )//delta == 0
		{
			solution1.copy(solution0);
		}
		else
		{//if delta> 0
			solution1.x_circuit = (det * dy - sgn(dy)*dx*sqrt(delta)) / (dr*dr);
			solution1.y_circuit = (-det * dx - abs(dy)*sqrt(delta)) / (dr*dr);
		}
	}


	solution0.x_circuit += x_recentering_movement;
	solution0.y_circuit += y_recentering_movement;
	solution1.x_circuit += x_recentering_movement;
	solution1.y_circuit += y_recentering_movement;

	pair<point_2d, point_2d>result(solution0, solution1);
	
	return result;
}//end calculate circle line intersection



int check_point_on_segment(point_2d seg0_point0, point_2d seg0_point1, point_2d third_point )
{
	double det =	(seg0_point0.x_circuit- seg0_point1.x_circuit)*(seg0_point1 .y_circuit- third_point.y_circuit) - 
					(seg0_point0.y_circuit- seg0_point1.y_circuit) * (seg0_point1.x_circuit- third_point.x_circuit);


	if ((0 - FLOAT_EQ_CT < det) && (det< 0+FLOAT_EQ_CT) )
	{
		return 1;
	}
	else
	{
		return 0;
	}
}



pair<point_2d, point_2d> calculate_acute_angle_bisector(point_2d left_point, point_2d middle_point, point_2d right_point)
{
	//from https://www.quora.com/What-is-the-equation-of-the-acute-angle-bisector-of-two-straight-lines

	// a1 x + b1 y + c1 = 0 ; a2 x + b2 y + c2 = 0
	// y = m1 x + m1c; y = m2 x + m2c
	//calculate coefficients
	double a1, b1, c1;
	double a2, b2, c2;

	a1 = left_point.y_circuit - middle_point.y;
	b1 = middle_point.x - left_point.x_circuit ;
	c1 = (left_point.x_circuit * middle_point.y) - (middle_point.x * left_point.y_circuit);

	a2 = middle_point.y - right_point.y_circuit;
	b2 = right_point.x_circuit - middle_point.x ;
	c2 = (middle_point.x * right_point.y_circuit) - (right_point.x_circuit * middle_point.y);

	
	if (sgn(c1) != sgn(c2)) // if signs differ, invert one of the equations
	{
		c2 = -c2;
		b2 = -b2;
		a2 = -a2;
	}
	double s = sqrt(a2*a2 + b2*b2);
	double t = sqrt(a1*a1 + b1*b1);
	

	double a3_0 = a1 * s - a2 * t;
	double b3_0 = b1 * s - b2 * t;
	double c3_0 = c1 * s - c2 * t;

	double a3_1 = a1 * s + a2 * t;
	double b3_1 = b1 * s + b2 * t;
	double c3_1 = c1 * s + c2 * t;

	if (b3_0 == 0)
	{
		b3_0 += FLOAT_EQ_CT;
	}
	if (b3_1 == 0)
	{
		b3_1 += FLOAT_EQ_CT;
	}
	

	point_2d second_point_from_line0;
	point_2d second_point_from_line1;

	second_point_from_line0.copy(middle_point);
	second_point_from_line0.x = middle_point.x + 1;
	second_point_from_line0.x_circuit = second_point_from_line0.x;
	second_point_from_line0.y = (-a3_0 / b3_0)*second_point_from_line0.x - c3_0/b3_0;
	second_point_from_line0.y_circuit = second_point_from_line0.y;

	second_point_from_line1.copy(middle_point);
	second_point_from_line1.x = middle_point.x + 1;
	second_point_from_line1.x_circuit = second_point_from_line1.x;
	second_point_from_line1.y = (-a3_1 / b3_1)*second_point_from_line1.x - c3_1 / b3_1;
	second_point_from_line1.y_circuit = second_point_from_line1.y;

	pair<point_2d, point_2d> solutions_from_line0;
	pair<point_2d, point_2d> solutions_from_line1;

	solutions_from_line0 = calculate_circle_line_intersection(middle_point, second_point_from_line0, middle_point);
	solutions_from_line1 = calculate_circle_line_intersection(middle_point, second_point_from_line1, middle_point);

	solutions_from_line0.first.x = solutions_from_line0.first.x_circuit;
	solutions_from_line0.second.x = solutions_from_line0.second.x_circuit;
	solutions_from_line1.first.x = solutions_from_line1.first.x_circuit;
	solutions_from_line1.second.x = solutions_from_line1.second.x_circuit;
	solutions_from_line0.first.y = solutions_from_line0.first.y_circuit;
	solutions_from_line0.second.y = solutions_from_line0.second.y_circuit;
	solutions_from_line1.first.y = solutions_from_line1.first.y_circuit;
	solutions_from_line1.second.y = solutions_from_line1.second.y_circuit;

	//find best point from all the 4 intersections
	double best_distance_sum;
	int best_line ;
	double temp_sum = calculate_distance(left_point, solutions_from_line0.first)+ calculate_distance(right_point, solutions_from_line0.first);
	best_distance_sum = temp_sum;
	best_line = 0;
	point_2d second;
	second.copy(solutions_from_line0.first);

	temp_sum = calculate_distance(left_point, solutions_from_line0.second) + calculate_distance(right_point, solutions_from_line0.second);

	if (best_distance_sum > temp_sum)
	{
		best_distance_sum = temp_sum;
		best_line = 0;
		second.copy(solutions_from_line0.second);
	}
	temp_sum = calculate_distance(left_point, solutions_from_line1.first) + calculate_distance(right_point, solutions_from_line1.first);
	if (best_distance_sum > temp_sum)
	{
		best_distance_sum = temp_sum;
		best_line = 1;
		second.copy(solutions_from_line1.first);
	}
	temp_sum = calculate_distance(left_point, solutions_from_line1.second) + calculate_distance(right_point, solutions_from_line1.second);
	if (best_distance_sum > temp_sum)
	{
		best_distance_sum = temp_sum;
		best_line = 1;
		second.copy(solutions_from_line1.second);
	}

	second.x_circuit = second.x;
	second.y_circuit = second.y;


	

	pair<point_2d, point_2d> result(middle_point,second);
	return result;
}


//varianta cu bisectoarea unghiului , caut punct de la intersectia cerc cu bisectoare - bisector method
//*
point_2d optimize_points_rubber_band_5(point_2d left_point, point_2d right_point, point_2d middle_point)
{
	point_2d best_point;

	//cazurile de egalitate intre puncte trebuie tratate separat:
	

	if ((left_point.check_equals_xy_circuit(middle_point) == 1)|| (right_point.check_equals_xy_circuit(middle_point) == 1))
	{
		return middle_point;
	}


	//check daca dreapta este inclusa in cerc cu totul
	double distance_point_left_center = calculate_distance(left_point, middle_point);
	double distance_point_right_center = calculate_distance(right_point, middle_point);
	if ((distance_point_left_center < middle_point.radius) && (distance_point_right_center < middle_point.radius))
	{
		best_point.copy(middle_point);
		best_point.x_circuit = right_point.x_circuit;
		best_point.y_circuit = right_point.y_circuit;
		return best_point;
	}


	if (left_point.check_equals_xy_circuit(right_point) == 1)
	{
		pair<point_2d, point_2d> solutions(calculate_circle_line_intersection(left_point, middle_point, middle_point));
		//check which solution is better
		double distance_sum0 = calculate_distance(left_point, solutions.first) + calculate_distance(solutions.first, right_point);
		double distance_sum1 = calculate_distance(left_point, solutions.second) + calculate_distance(solutions.second, right_point);
		if (distance_sum0 < distance_sum1)
		{
			return solutions.first;
		}
		else
		{
			return solutions.second;
		}
	}


	//check daca dreapta trece prin cerc direct => solutie direct punctele de intersectie
		//calc distanta centru cerc <-> segment
			//if smaller 
				//gaseste cele 2 puncte de intersectie cu cercul => dai return
			//if larger => 
				//calculeaza ecuatia bisectoarei unghiului
				//calc intersectia dintre bisectoare si cerc
				//vezi care din cele 2 solutii e mai buna
	double temp_double = calculate_distance_point_center_segment(left_point,right_point,middle_point);

	if (temp_double <= middle_point.radius)
	{
		pair<point_2d, point_2d> solutions(calculate_circle_line_intersection(left_point,right_point,middle_point));
		//check which solution is better
		double distance_sum0 = calculate_distance(left_point, solutions.first)+ calculate_distance(solutions.first,right_point);
		double distance_sum1 = calculate_distance(left_point, solutions.second) + calculate_distance(solutions.second, right_point);
		if (distance_sum0< distance_sum1)
		{
			return solutions.first;
		}
		else
		{
			return solutions.second;
		}
	}
	else//if line does not intersect circle
	{
		//calculate acute angle bisector
		pair<point_2d, point_2d> acute_angle_bisector = calculate_acute_angle_bisector(left_point, middle_point, right_point);
		
		//acum calculate_acute_angle_bisector da direct punctul de pe cerc
			//asa ca pot da:
		acute_angle_bisector.second.x = acute_angle_bisector.first.x;
		acute_angle_bisector.second.y = acute_angle_bisector.first.y;
		acute_angle_bisector.second.index = acute_angle_bisector.first.index;
		acute_angle_bisector.second.radius = acute_angle_bisector.first.radius;
		return acute_angle_bisector.second;
		
		
	}

	


	return best_point;
}//end point_2d optimize_points_rubber_band_5(point_2d left_point, point_2d right_point, point_2d middle_point)
//*/


point_2d calculate_segment_middle_point(point_2d left_point, point_2d right_point)
{
	point_2d result;

	result.index = -1;
	result.x = (left_point.x + right_point.x)/2;
	result.y = (left_point.y + right_point.y) / 2;
	result.radius = 0;
	result.x_circuit = (left_point.x_circuit + right_point.x_circuit) / 2;
	result.y_circuit = (left_point.y_circuit + right_point.y_circuit) / 2;

	return result;
}


//varianta cu ceva = mijlocul segmentului left-right si intersectia cerc seg middle-ceva - middle point from segment method
//*
point_2d optimize_points_rubber_band_6(point_2d left_point, point_2d right_point, point_2d middle_point)
{
	point_2d best_point;

	if ((left_point.check_equals_xy_circuit(middle_point) == 1) || (right_point.check_equals_xy_circuit(middle_point) == 1))
	{
		return middle_point;
	}


	//check daca dreapta este inclusa in cerc cu totul
	double distance_point_left_center = calculate_distance(left_point, middle_point);
	double distance_point_right_center = calculate_distance(right_point, middle_point);
	if ((distance_point_left_center < middle_point.radius) && (distance_point_right_center < middle_point.radius))
	{
		best_point.copy(middle_point);
		best_point.x_circuit = right_point.x_circuit;
		best_point.y_circuit = right_point.y_circuit;
		return best_point;
	}


	if (left_point.check_equals_xy_circuit(right_point) == 1)
	{
		pair<point_2d, point_2d> solutions(calculate_circle_line_intersection(left_point, middle_point, middle_point));
		//check which solution is better
		double distance_sum0 = calculate_distance(left_point, solutions.first) + calculate_distance(solutions.first, right_point);
		double distance_sum1 = calculate_distance(left_point, solutions.second) + calculate_distance(solutions.second, right_point);
		if (distance_sum0 < distance_sum1)
		{
			return solutions.first;
		}
		else
		{
			return solutions.second;
		}
	}




	//check daca dreapta trece prin cerc direct => solutie direct punctele de intersectie
		//calc distanta centru cerc <-> segment
			//if smaller 
				//gaseste cele 2 puncte de intersectie cu cercul => dai return
			//if larger => 
				//find middle point from segment
				//intersect (segment_middle_point,circle_center) with circle
	double temp_double = calculate_distance_point_center_segment(left_point, right_point, middle_point);

	if (temp_double <= middle_point.radius)
	{
		pair<point_2d, point_2d> solutions(calculate_circle_line_intersection(left_point, right_point, middle_point));
		//check which solution is better
		double distance_sum0 = calculate_distance(left_point, solutions.first) + calculate_distance(solutions.first, right_point);
		double distance_sum1 = calculate_distance(left_point, solutions.second) + calculate_distance(solutions.second, right_point);
		if (distance_sum0 < distance_sum1)
		{
			best_point.copy(solutions.first);
		}
		else
		{
			best_point.copy(solutions.second);
		}
	}
	else//if line does not intersect circle
	{
		point_2d segment_middle_point = calculate_segment_middle_point(left_point,right_point);

		point_2d temp_middle_point;
		temp_middle_point.copy(middle_point);
		temp_middle_point.x_circuit = temp_middle_point.x;
		temp_middle_point.y_circuit = temp_middle_point.y;
		pair<point_2d, point_2d> solutions(calculate_circle_line_intersection(segment_middle_point, temp_middle_point, middle_point));
		//check which solution is better
		double distance_sum0 = calculate_distance(left_point, solutions.first) + calculate_distance(solutions.first, right_point);
		double distance_sum1 = calculate_distance(left_point, solutions.second) + calculate_distance(solutions.second, right_point);
		if (distance_sum0 < distance_sum1)
		{
			best_point.copy(solutions.first);
		}
		else
		{
			best_point.copy(solutions.second);
		}

	}




	return best_point;
}//end point_2d optimize_points_rubber_band_5(point_2d left_point, point_2d right_point, point_2d middle_point)
//*/





//varianta cu golden section search modified- middle section search
//*
point_2d optimize_points_rubber_band_7(point_2d left_point, point_2d right_point, point_2d middle_point)
{
	point_2d best_point;
	best_point.copy(middle_point);
	double step = middle_point.radius;
	double current_x = middle_point.x;

	while (step > FLOAT_EQ_CT)
	{
		//test point middle - radius
		double test_x1 = current_x - step;
		point_2d best_point1;
		if ((test_x1 < middle_point.x - middle_point.radius) || ((test_x1 > middle_point.x + middle_point.radius)))
		{//if point out of bounds
			best_point1.copy(best_point);
		}
		else
		{
			double a1, b1, c1;
			a1 = 1;
			b1 = -2 * middle_point.y;
			c1 = test_x1 - middle_point.x;
			c1 = c1 * c1 - middle_point.radius * middle_point.radius + middle_point.y*middle_point.y;
			pair<double, double> y_pair1 = solve_second_order_equation(a1, b1, c1);
			pair<point_2d, point_2d> point_pair1;
			point_pair1.first.x_circuit = test_x1;
			point_pair1.first.y_circuit = y_pair1.first;
			point_pair1.second.x_circuit = test_x1;
			point_pair1.second.y_circuit = y_pair1.second;
			best_point1.copy(middle_point);
			best_point1.x_circuit = test_x1;
			if (
				(calculate_distance(left_point, point_pair1.first) + calculate_distance(point_pair1.first, right_point)) <
				(calculate_distance(left_point, point_pair1.second) + calculate_distance(point_pair1.second, right_point))
				)
			{
				best_point1.y_circuit = point_pair1.first.y_circuit;
			}
			else
			{
				best_point1.y_circuit = point_pair1.second.y_circuit;
			}
		}
		


		//test point middle + radius
		double test_x2 = current_x + step;
		point_2d best_point2;
		if ((test_x2 < middle_point.x - middle_point.radius) || ((test_x2 > middle_point.x + middle_point.radius)))
		{//if point out of bounds
			best_point2.copy(best_point);
		}
		else
		{
			double a2, b2, c2;
			a2 = 1;
			b2 = -2 * middle_point.y;
			c2 = test_x2 - middle_point.x;
			c2 = c2 * c2 - middle_point.radius * middle_point.radius + middle_point.y*middle_point.y;
			pair<double, double> y_pair2 = solve_second_order_equation(a2, b2, c2);
			pair<point_2d, point_2d> point_pair2;
			point_pair2.first.x_circuit = test_x2;
			point_pair2.first.y_circuit = y_pair2.first;
			point_pair2.second.x_circuit = test_x2;
			point_pair2.second.y_circuit = y_pair2.second;
			best_point2.copy(middle_point);
			best_point2.x_circuit = test_x2;
			if (
				(calculate_distance(left_point, point_pair2.first) + calculate_distance(point_pair2.first, right_point)) <
				(calculate_distance(left_point, point_pair2.second) + calculate_distance(point_pair2.second, right_point))
				)
			{
				best_point2.y_circuit = point_pair2.first.y_circuit;
			}
			else
			{
				best_point2.y_circuit = point_pair2.second.y_circuit;
			}
		}


		if (
				(calculate_distance(left_point, best_point1) + calculate_distance(best_point1, right_point)) <
				(calculate_distance(left_point, best_point2) + calculate_distance(best_point2, right_point))
			)
		{
			current_x = best_point1.x_circuit;
			best_point.x_circuit = current_x;
			best_point.y_circuit = best_point1.y_circuit;
		}
		else
		{
			current_x = best_point2.x_circuit;
			best_point.x_circuit = current_x;
			best_point.y_circuit = best_point2.y_circuit;
		}
		step = step / 2;

	}

	
	
	return best_point;

}//end point_2d optimize_points_rubber_band_7(point_2d left_point, point_2d right_point, point_2d middle_point)
//*/




vector<point_2d> optimize_circuit_rubber_band(vector<point_2d> initial_circuit, point_2d(*selected_optimizer)(point_2d,point_2d,point_2d))
{
	int nr_iterations = 0;
	vector<point_2d> previous_circuit(initial_circuit);
	vector<point_2d> current_circuit(previous_circuit);
	double current_distance = 0;
	double previous_distance = 0; 
	do
	{// until circuit stops improving (or improves too little)	
		previous_circuit = current_circuit;
		//error de la 2 la 3
		for (unsigned int i = 0; i < current_circuit.size() - 2; i++)
		{
			current_circuit[i+1].copy(selected_optimizer(current_circuit[i], current_circuit[i+2],current_circuit[i+1]));
		}
		current_circuit[0].copy(selected_optimizer(current_circuit[current_circuit.size()-1], current_circuit[1], current_circuit[0]));
		current_circuit[current_circuit.size()-1].copy(selected_optimizer(current_circuit[current_circuit.size()-2], current_circuit[0], current_circuit[current_circuit.size()-1]));
		current_distance = calculate_total_distance(current_circuit);
		previous_distance = calculate_total_distance(previous_circuit);
		nr_iterations++;
	} while ((abs(current_distance - previous_distance) > FLOAT_EQ_CT) && (nr_iterations <= MAX_NR_ITERATIONS) ) ;

	return current_circuit;
}// end vector<point_2d> optimize_circuit_rubber_band(vector<point_2d> initial_circuit, float stop_limit)




class results_1_optimizers
{
public:
	long long time;
	double time_relative_error;
	double circuit_length;

	double absolute_error;
	double relative_error;

	results_1_optimizers()
	{
		
	}

	void calculate_errors(results_1_optimizers best)
	{
		absolute_error = abs(circuit_length - best.circuit_length);
		relative_error = (absolute_error / best.circuit_length) * 100;
		time_relative_error = (abs(time - best.time)*1.0 / best.time) * 100;
	}

	void set_all(long long  arg_time, double arg_circuit_length, double arg_absolute_error, double arg_relative_error )
	{
		time = arg_time;
		circuit_length = arg_circuit_length;
		absolute_error = arg_absolute_error;
		relative_error = arg_relative_error;
	}

	string to_string()
	{
		return "time: " + std::to_string((long long)time) + " time_relative_error: " + std::to_string((long long)time_relative_error)
			+ " circuit_length: " + std::to_string((long double)circuit_length)
			+ " absolute_error: " + std::to_string((long double)absolute_error) + " relative_error: " + std::to_string((long double)relative_error);
	}

};


class results_all_optimizers
{
public:
	results_1_optimizers starting_circuit;
	results_1_optimizers optimizer4;//used as reference
	results_1_optimizers optimizer5;
	results_1_optimizers optimizer6;
	results_1_optimizers optimizer7;

	results_all_optimizers()
	{

	}

	void calculate_errors()
	{
		results_1_optimizers min;
		min.circuit_length = optimizer4.circuit_length;
		min.time = optimizer4.time;
				//distance
		if (starting_circuit.circuit_length < min.circuit_length)
		{
			min.circuit_length = starting_circuit.circuit_length;
		}
		if (optimizer5.circuit_length < min.circuit_length)
		{
			min.circuit_length = optimizer5.circuit_length;
		}
		if (optimizer6.circuit_length < min.circuit_length)
		{
			min.circuit_length = optimizer6.circuit_length;
		}
		if (optimizer7.circuit_length < min.circuit_length)
		{
			min.circuit_length = optimizer7.circuit_length;
		}

		if (min.circuit_length < FLOAT_EQ_CT)
		{
			min.circuit_length = FLOAT_EQ_CT;
		}

				//time
		if (starting_circuit.time < min.time)
		{
			min.time = starting_circuit.time;
		}
		if (optimizer5.time < min.time)
		{
			min.time = optimizer5.time;
		}
		if (optimizer6.time < min.time)
		{
			min.time = optimizer6.time;
		}
		if (optimizer7.time < min.time)
		{
			min.time = optimizer7.time;
		}

		if (min.circuit_length < FLOAT_EQ_CT)
		{
			min.circuit_length = FLOAT_EQ_CT;
		}



		optimizer4.calculate_errors(min);
		optimizer5.calculate_errors(min);
		optimizer6.calculate_errors(min);
		optimizer7.calculate_errors(min);
		starting_circuit.calculate_errors(min);
	}

	string to_string()
	{
		return +"starting_circuit: " + starting_circuit.to_string() + "\n"
			+ "optimizer4: " + optimizer4.to_string() + "\n"
			+ "optimizer5: " + optimizer5.to_string() + "\n"
			+ "optimizer6: " + optimizer6.to_string() + "\n"
			+ "optimizer6: " + optimizer7.to_string() + "\n";
	}

};






int main(int argc, char *argv[])
{

	vector<point_2d> points;
	vector<point_2d> optimized_circuit_points;
	vector<results_all_optimizers> results0;
	ofstream test_suite_results;
	ofstream individual_test_results;
	long long duration = 0;
	chrono::high_resolution_clock::time_point t1;
	chrono::high_resolution_clock::time_point t2;





	/*
	point_2d temp;
	vector<point_2d> points4, points5, points6, points7;
	temp.set_point_2d(0, 0, 0, 10, 0, 0);
	points.push_back(temp);
	temp.set_point_2d(1,3, -25, 10, 3, -25);
	points.push_back(temp);
	temp.set_point_2d(2, 5, -50, 10, 5, -50);
	points.push_back(temp);
	temp.set_point_2d(0, 20, -80, 10, 20, -80);
	points.push_back(temp);
	temp.set_point_2d(0, 50, 0, 10, 50, 0);
	points.push_back(temp);
	temp.set_point_2d(0, 50, 50, 10, 50, 50);
	points.push_back(temp);
	temp.set_point_2d(0, 0, 50, 10, 0, 50);
	points.push_back(temp);





	points4 = optimize_circuit_rubber_band(points, optimize_points_rubber_band_4);
	double distance_4 = calculate_total_distance(points4);
	points5 = optimize_circuit_rubber_band(points, optimize_points_rubber_band_5);
	double distance_5 = calculate_total_distance(points5);
	points6 = optimize_circuit_rubber_band(points, optimize_points_rubber_band_6);
	double distance_6 = calculate_total_distance(points6);
	points7 = optimize_circuit_rubber_band(points, optimize_points_rubber_band_7);
	double distance_7 = calculate_total_distance(points7);


	//*/


	switch (argc)
	{
		case(1):
		{
			FLOAT_EQ_CT = 0.00001;
			SEED = 1;
			NR_TESTS = 1000;
			MIN_RADIUS = 5;
			MAX_RADIUS = 5;
			MIN_POINTS = 3;
			MAX_POINTS = 3;
			MAX_X = 100;
			MAX_Y = 100;
			MAX_NR_ITERATIONS = 10000;
			break;
		}
		case(11):
		{
			FLOAT_EQ_CT = atof(argv[1]);
			SEED = atoi(argv[2]);
			NR_TESTS = atoi(argv[3]);
			MIN_RADIUS = atoi(argv[4]);
			MAX_RADIUS = atoi(argv[5]);
			MIN_POINTS = atoi(argv[6]);
			MAX_POINTS = atoi(argv[7]);
			MAX_X = atoi(argv[8]);
			MAX_Y = atoi(argv[9]);
			MAX_NR_ITERATIONS = atoi(argv[10]);
			break;
		}
		default:
		{
			cout << "ERROR: improper number of arguments used";
			return 1;
			break;
		}
			
	}

	
	srand(SEED); 
	test_suite_results.open("test_suite_results.txt");
	individual_test_results.open("individual_test_results.txt");
	test_suite_results << "STARTING TEST SUITE\n\n";
	
	//print test settings 
	test_suite_results << "test settings:\n";
	test_suite_results << "FLOAT_EQ_CT " << FLOAT_EQ_CT << endl;
	test_suite_results << "SEED " << SEED << endl;
	test_suite_results << "NR_TESTS " << NR_TESTS << endl;
	test_suite_results << "MIN_RADIUS " << MIN_RADIUS << endl;
	test_suite_results << "MAX_RADIUS " << MAX_RADIUS << endl;
	test_suite_results << "MIN_POINTS " << MIN_POINTS << endl;
	test_suite_results << "MAX_POINTS " << MAX_POINTS << endl;
	test_suite_results << "MAX_X " << MAX_X << endl;
	test_suite_results << "MAX_Y " << MAX_Y << endl;

	cout << "STARTING TEST SUITE\n\n";
	//print test settings 
	cout << "test settings:\n";
	cout << "FLOAT_EQ_CT " << FLOAT_EQ_CT << endl;
	cout << "SEED " << SEED << endl;
	cout << "NR_TESTS " << NR_TESTS << endl;
	cout << "MIN_RADIUS " << MIN_RADIUS << endl;
	cout << "MAX_RADIUS " << MAX_RADIUS << endl;
	cout << "MIN_POINTS " << MIN_POINTS << endl;
	cout << "MAX_POINTS " << MAX_POINTS << endl;
	cout << "MAX_X " << MAX_X << endl;
	cout << "MAX_Y " << MAX_Y << endl;

	test_suite_results << endl << endl;
	cout << endl << endl;
	
	
	
	
	for (int i = 0; i < NR_TESTS; i++)
	{
		int nr_points;
		nr_points = MIN_POINTS + rand() % (MAX_POINTS - MIN_POINTS + 1);

		test_suite_results << endl << endl << endl << "test nr " << i << " of " << NR_TESTS << "; nr_points " << nr_points << endl;
		individual_test_results << endl << endl << endl << "test nr " << i <<" of " << NR_TESTS << "; nr_points " << nr_points << endl;
		cout << endl << endl << endl << "test nr " << i << " of " << NR_TESTS <<"; nr_points "<< nr_points << endl;
		
		//generate population
		points.clear();
		for (int j = 0; j < nr_points; j++)
		{
			point_2d temp;
			temp.index = j;
			temp.radius = MIN_RADIUS + rand() % (MAX_RADIUS- MIN_RADIUS+1);
			temp.x = rand() % MAX_X;
			temp.y = rand() % MAX_Y;
			temp.x_circuit = temp.x;
			temp.y_circuit = temp.y;
			points.push_back(temp);
		}

		//save population parameters - daca mai vreau sa am
			//mean distance between points
			//mean radius
			//largest discrepancies ?
			//others


		//run test with each function
			//call function
			//save result
		
		results_all_optimizers temp_results_all_optimizers;


		//starting_circuit
		temp_results_all_optimizers.starting_circuit.set_all(99999999, calculate_total_distance(points), 0, 0);
		individual_test_results << circuit_to_string(points);
		//cout << circuit_to_string(points);
		optimized_circuit_points.clear();


		//optimizer 4
		t1 = chrono::high_resolution_clock::now();
		optimized_circuit_points = optimize_circuit_rubber_band(points, optimize_points_rubber_band_4);
		//optimized_circuit_points = points; // for profiling uncomment this, comment above
		t2 = chrono::high_resolution_clock::now();
		duration = chrono::duration_cast<chrono::microseconds>(t2 - t1).count();
		temp_results_all_optimizers.optimizer4.set_all(duration,calculate_total_distance(optimized_circuit_points),0,0);
		//individual_test_results << circuit_to_string(optimized_circuit_points);
		//cout << circuit_to_string(optimized_circuit_points);
		optimized_circuit_points.clear();

		//optimizer 5
		t1 = chrono::high_resolution_clock::now();
		optimized_circuit_points = optimize_circuit_rubber_band(points, optimize_points_rubber_band_5);
		t2 = chrono::high_resolution_clock::now();
		duration = chrono::duration_cast<chrono::microseconds>(t2 - t1).count();
		temp_results_all_optimizers.optimizer5.set_all(duration, calculate_total_distance(optimized_circuit_points), 0, 0);
		//individual_test_results << circuit_to_string(optimized_circuit_points);
		//cout << circuit_to_string(optimized_circuit_points);
		optimized_circuit_points.clear();

		//optimizer 6
		t1 = chrono::high_resolution_clock::now();
		optimized_circuit_points = optimize_circuit_rubber_band(points, optimize_points_rubber_band_6);
		t2 = chrono::high_resolution_clock::now();
		duration = chrono::duration_cast<chrono::microseconds>(t2 - t1).count();
		temp_results_all_optimizers.optimizer6.set_all(duration, calculate_total_distance(optimized_circuit_points), 0, 0);
		//individual_test_results << circuit_to_string(optimized_circuit_points);
		//cout << circuit_to_string(optimized_circuit_points);
		optimized_circuit_points.clear();

		//optimizer 7
		t1 = chrono::high_resolution_clock::now();
		optimized_circuit_points = optimize_circuit_rubber_band(points, optimize_points_rubber_band_7);
		t2 = chrono::high_resolution_clock::now();
		duration = chrono::duration_cast<chrono::microseconds>(t2 - t1).count();
		temp_results_all_optimizers.optimizer7.set_all(duration, calculate_total_distance(optimized_circuit_points), 0, 0);
		//individual_test_results << circuit_to_string(optimized_circuit_points);
		//cout << circuit_to_string(optimized_circuit_points);
		optimized_circuit_points.clear();


		temp_results_all_optimizers.calculate_errors();


		results0.push_back(temp_results_all_optimizers);
		

		cout << results0[i].to_string()<<endl;
		individual_test_results << results0[i].to_string()<<endl;
		test_suite_results << results0[i].to_string()<<endl;


	}

	//calculate info over all tests run
	//calculate largest relative error for each method - length
	double largest_relative_error_for_brute_force_method = 0;
	double largest_relative_error_for_bisection_method = 0;
	double largest_relative_error_for_middle_point_method = 0;
	double largest_relative_error_for_golden_section_method = 0;
	double mean_relative_error_for_brute_force_method = 0;
	double mean_relative_error_for_bisection_method = 0;
	double mean_relative_error_for_middle_point_method = 0;
	double mean_relative_error_for_golden_section_method = 0;

	for (int i = 0; i < results0.size(); i++)
	{
		mean_relative_error_for_brute_force_method += results0[i].optimizer4.relative_error;
		mean_relative_error_for_bisection_method += results0[i].optimizer5.relative_error;
		mean_relative_error_for_middle_point_method += results0[i].optimizer6.relative_error;
		mean_relative_error_for_golden_section_method += results0[i].optimizer7.relative_error;
		if (largest_relative_error_for_brute_force_method < results0[i].optimizer4.relative_error)
		{
			largest_relative_error_for_brute_force_method = results0[i].optimizer4.relative_error;
		}
		if (largest_relative_error_for_bisection_method < results0[i].optimizer5.relative_error)
		{
			largest_relative_error_for_bisection_method = results0[i].optimizer5.relative_error;
		}
		if (largest_relative_error_for_middle_point_method < results0[i].optimizer6.relative_error)
		{
			largest_relative_error_for_middle_point_method = results0[i].optimizer6.relative_error;
		}
		if (largest_relative_error_for_middle_point_method < results0[i].optimizer7.relative_error)
		{
			largest_relative_error_for_golden_section_method = results0[i].optimizer7.relative_error;
		}
	}
	mean_relative_error_for_bisection_method /= results0.size();
	mean_relative_error_for_middle_point_method /= results0.size();
	mean_relative_error_for_brute_force_method /= results0.size();
	mean_relative_error_for_golden_section_method /= results0.size();

	cout << endl << endl << "largest_relative_error_for_brute_force_method: " << largest_relative_error_for_brute_force_method << endl;
	cout << endl << endl << "largest_relative_error_for_bisection_method: " << largest_relative_error_for_bisection_method<<endl;
	cout << endl << endl << "largest_relative_error_for_middle_point_method: " << largest_relative_error_for_middle_point_method << endl;
	cout << endl << endl << "largest_relative_error_for_golden_section_method: " << largest_relative_error_for_golden_section_method << endl;

	test_suite_results << endl << endl << "largest_relative_error_for_brute_force_method: " << largest_relative_error_for_brute_force_method << endl;
	test_suite_results << endl << endl << "largest_relative_error_for_bisection_method: " << largest_relative_error_for_bisection_method << endl;
	test_suite_results << endl << endl << "largest_relative_error_for_middle_point_method: " << largest_relative_error_for_middle_point_method << endl;
	test_suite_results << endl << endl << "largest_relative_error_for_golden_section_method: " << largest_relative_error_for_golden_section_method << endl;


	cout << endl << endl << "mean_relative_error_for_brute_force_method: " << mean_relative_error_for_brute_force_method << endl;
	cout << endl << endl << "mean_relative_error_for_bisection_method: " << mean_relative_error_for_bisection_method << endl;
	cout << endl << endl << "mean_relative_error_for_middle_point_method: " << mean_relative_error_for_middle_point_method << endl;
	cout << endl << endl << "mean_relative_error_for_golden_section_method: " << mean_relative_error_for_golden_section_method << endl;

	test_suite_results << endl << endl << "mean_relative_error_for_brute_force_method: " << mean_relative_error_for_brute_force_method << endl;
	test_suite_results << endl << endl << "mean_relative_error_for_bisection_method: " << mean_relative_error_for_bisection_method << endl;
	test_suite_results << endl << endl << "mean_relative_error_for_middle_point_method: " << mean_relative_error_for_middle_point_method << endl;
	test_suite_results << endl << endl << "mean_relative_error_for_golden_section_method: " << mean_relative_error_for_golden_section_method << endl;

	//relative error histogram, 0.5% intervale - length
		//v[0] = 0<->0.5%; v[1] = 0.5%<->1% etc
		
	//starting_circuit
	vector<int> starting_circuit_histogram;
	for (int i = 0; i < 201; i++)
	{
		starting_circuit_histogram.push_back(0);
	}
	for (int i = 0; i < results0.size(); i++)
	{
		double nr = results0[i].starting_circuit.relative_error;
		int index = int(floor(nr * 2));
		if (index >= 201)
		{
			starting_circuit_histogram[200] += 1;
		}
		else 
		{
			starting_circuit_histogram[index] += 1;
		}
		
	}

	//bisection method
	vector<int> bisection_method_histogram;
	for (int i = 0; i < 201; i++)
	{
		bisection_method_histogram.push_back(0);
	}
	for (int i = 0; i < results0.size(); i++)
	{
		double nr = results0[i].optimizer5.relative_error;
		int index = int(floor(nr * 2));
		if (index >= 200) 
		{
			bisection_method_histogram[200] += 1;
		}
		else
		{
			bisection_method_histogram[index] += 1;
		}
		
	}

	//middle point method
	vector<int> middle_point_method_histogram;
	for (int i = 0; i < 201; i++)
	{
		middle_point_method_histogram.push_back(0);
	}
	for (int i = 0; i < results0.size(); i++)
	{
		double nr = results0[i].optimizer6.relative_error;
		int index = int(floor(nr * 2));
		if (index >= 200)
		{
			middle_point_method_histogram[200] += 1;
		}
		else
		{
			middle_point_method_histogram[index] += 1;
		}
	}

	//golden section method
	vector<int> golden_section_method_histogram;
	for (int i = 0; i < 201; i++)
	{
		golden_section_method_histogram.push_back(0);
	}
	for (int i = 0; i < results0.size(); i++)
	{
		double nr = results0[i].optimizer7.relative_error;
		int index = int(floor(nr * 2));
		if (index >= 200)
		{
			golden_section_method_histogram[200] += 1;
		}
		else
		{
			golden_section_method_histogram[index] += 1;
		}
	}


	test_suite_results << endl << endl << endl << endl << "RELATIVE ERROR HISTOGRAMS" << endl;
	test_suite_results << endl << "starting_circuit_histogram" << endl;
	for (int i = 0; i < starting_circuit_histogram.size(); i++)
	{
		double lower_end = i * 0.5;
		double upper_end = lower_end+0.5;
		test_suite_results << "interval: " << lower_end << "% " << upper_end << "% : " << starting_circuit_histogram[i] << endl;
	}

	test_suite_results << endl << "bisection_method_histogram" << endl;
	for (int i = 0; i < bisection_method_histogram.size(); i++)
	{
		double lower_end = i * 0.5;
		double upper_end = lower_end + 0.5;
		test_suite_results << "interval: " << lower_end << "% " << upper_end << "% : " << bisection_method_histogram[i] << endl;
	}

	test_suite_results << endl << "middle_point_method_histogram" << endl;
	for (int i = 0; i < middle_point_method_histogram.size(); i++)
	{
		double lower_end = i * 0.5;
		double upper_end = lower_end + 0.5;
		test_suite_results << "interval: " << lower_end << "% " << upper_end << "% : " << middle_point_method_histogram[i] << endl;
	}

	test_suite_results << endl << "golden_section_method_histogram" << endl;
	for (int i = 0; i < golden_section_method_histogram.size(); i++)
	{
		double lower_end = i * 0.5;
		double upper_end = lower_end + 0.5;
		test_suite_results << "interval: " << lower_end << "% " << upper_end << "% : " << golden_section_method_histogram[i] << endl;
	}



			//calculate largest relative error for each method - time
	double largest_time_relative_error_for_brute_force_method = 0;
	double largest_time_relative_error_for_bisection_method = 0;
	double largest_time_relative_error_for_middle_point_method = 0;
	double largest_time_relative_error_for_golden_section_method = 0;
	double mean_time_relative_error_for_brute_force_method = 0;
	double mean_time_relative_error_for_bisection_method = 0;
	double mean_time_relative_error_for_middle_point_method = 0;
	double mean_time_relative_error_for_golden_section_method = 0;

	for (int i = 0; i < results0.size(); i++)
	{
		mean_time_relative_error_for_brute_force_method += results0[i].optimizer4.time_relative_error;
		mean_time_relative_error_for_bisection_method += results0[i].optimizer5.time_relative_error;
		mean_time_relative_error_for_middle_point_method += results0[i].optimizer6.time_relative_error;
		mean_time_relative_error_for_golden_section_method += results0[i].optimizer7.time_relative_error;
		if (largest_time_relative_error_for_brute_force_method < results0[i].optimizer4.time_relative_error)
		{
			largest_time_relative_error_for_brute_force_method = results0[i].optimizer4.time_relative_error;
		}
		if (largest_time_relative_error_for_bisection_method < results0[i].optimizer5.time_relative_error)
		{
			largest_time_relative_error_for_bisection_method = results0[i].optimizer5.time_relative_error;
		}
		if (largest_time_relative_error_for_middle_point_method < results0[i].optimizer6.time_relative_error)
		{
			largest_time_relative_error_for_middle_point_method = results0[i].optimizer6.time_relative_error;
		}
		if (largest_time_relative_error_for_golden_section_method < results0[i].optimizer7.time_relative_error)
		{
			largest_time_relative_error_for_golden_section_method = results0[i].optimizer7.time_relative_error;
		}
	}
	mean_time_relative_error_for_bisection_method /= results0.size();
	mean_time_relative_error_for_middle_point_method /= results0.size();
	mean_time_relative_error_for_brute_force_method /= results0.size();
	mean_time_relative_error_for_golden_section_method /= results0.size();

	cout << endl << endl << "largest_time_relative_error_for_brute_force_method: " << largest_time_relative_error_for_brute_force_method << endl;
	cout << endl << endl << "largest_time_relative_error_for_bisection_method: " << largest_time_relative_error_for_bisection_method << endl;
	cout << endl << endl << "largest_time_relative_error_for_middle_point_method: " << largest_time_relative_error_for_middle_point_method << endl;
	cout << endl << endl << "largest_time_relative_error_for_golden_section_method: " << largest_time_relative_error_for_golden_section_method << endl;

	test_suite_results << endl << endl << "largest_time_relative_error_for_brute_force_method: " << largest_time_relative_error_for_brute_force_method << endl;
	test_suite_results << endl << endl << "largest_time_relative_error_for_bisection_method: " << largest_time_relative_error_for_bisection_method << endl;
	test_suite_results << endl << endl << "largest_time_relative_error_for_middle_point_method: " << largest_time_relative_error_for_middle_point_method << endl;
	test_suite_results << endl << endl << "largest_time_relative_error_for_golden_section_method: " << largest_time_relative_error_for_golden_section_method << endl;


	cout << endl << endl << "mean_time_relative_error_for_brute_force_method: " << mean_time_relative_error_for_brute_force_method << endl;
	cout << endl << endl << "mean_time_relative_error_for_bisection_method: " << mean_time_relative_error_for_bisection_method << endl;
	cout << endl << endl << "mean_time_relative_error_for_middle_point_method: " << mean_time_relative_error_for_middle_point_method << endl;
	cout << endl << endl << "mean_time_relative_error_for_golden_section_method: " << mean_time_relative_error_for_golden_section_method << endl;

	test_suite_results << endl << endl << "mean_time_relative_error_for_brute_force_method: " << mean_time_relative_error_for_brute_force_method << endl;
	test_suite_results << endl << endl << "mean_time_relative_error_for_bisection_method: " << mean_time_relative_error_for_bisection_method << endl;
	test_suite_results << endl << endl << "mean_time_relative_error_for_middle_point_method: " << mean_time_relative_error_for_middle_point_method << endl;
	test_suite_results << endl << endl << "mean_time_relative_error_for_golden_section_method: " << mean_time_relative_error_for_golden_section_method << endl;

	//relative error histogram, 0.5% intervale - time
		//v[0] = 0<->0.5%; v[1] = 0.5%<->1% etc
	/*
	//starting_circuit
	vector<int> time_starting_circuit_histogram;
	int i = 0;
	for ( i= 0; i < 201; i++)
	{
		time_starting_circuit_histogram.push_back(0);
	}
	for ( i = 0; i < results0.size(); i++)
	{
		double nr = results0[i].starting_circuit.time_relative_error;
		int index = int(floor(nr * 2));
		if (index >= 201)
		{
			time_starting_circuit_histogram[200] += 1;
		}
		else
		{
			time_starting_circuit_histogram[index] += 1;
		}

	}

	//bisection method
	vector<int> time_bisection_method_histogram;
	for ( i = 0; i < 201; i++)
	{
		time_bisection_method_histogram.push_back(0);
	}
	for (int i = 0; i < results0.size(); i++)
	{
		double nr = results0[i].optimizer5.time_relative_error;
		int index = int(floor(nr * 2));
		if (index >= 200)
		{
			time_bisection_method_histogram[200] += 1;
		}
		else
		{
			time_bisection_method_histogram[index] += 1;
		}

	}

	//middle point method
	vector<int> time_middle_point_method_histogram;
	for ( i = 0; i < 201; i++)
	{
		time_middle_point_method_histogram.push_back(0);
	}
	for ( i = 0; i < results0.size(); i++)
	{
		double nr = results0[i].optimizer6.time_relative_error;
		int index = int(floor(nr * 2));
		if (index >= 200)
		{
			time_middle_point_method_histogram[200] += 1;
		}
		else
		{
			time_middle_point_method_histogram[index] += 1;
		}
	}

	//golden section method
	vector<int> time_golden_section_method_histogram;
	for ( i = 0; i < 201; i++)
	{
		time_golden_section_method_histogram.push_back(0);
	}
	for ( i = 0; i < results0.size(); i++)
	{
		double nr = results0[i].optimizer7.time_relative_error;
		int index = int(floor(nr * 2));
		if (index >= 200)
		{
			time_golden_section_method_histogram[200] += 1;
		}
		else
		{
			time_golden_section_method_histogram[index] += 1;
		}
	}


	test_suite_results << endl << endl << endl << endl << "RELATIVE TIME ERROR HISTOGRAMS" << endl;
	test_suite_results << endl << "starting_circuit_histogram" << endl;
	for ( i = 0; i < time_starting_circuit_histogram.size(); i++)
	{
		double lower_end = i * 0.5;
		double upper_end = lower_end + 0.5;
		test_suite_results << "interval: " << lower_end << "% " << upper_end << "% : " << time_starting_circuit_histogram[i] << endl;
	}

	test_suite_results << endl << "bisection_method_histogram" << endl;
	for ( i = 0; i < time_bisection_method_histogram.size(); i++)
	{
		double lower_end = i * 0.5;
		double upper_end = lower_end + 0.5;
		test_suite_results << "interval: " << lower_end << "% " << upper_end << "% : " << time_bisection_method_histogram[i] << endl;
	}

	test_suite_results << endl << "middle_point_method_histogram" << endl;
	for ( i = 0; i < time_middle_point_method_histogram.size(); i++)
	{
		double lower_end = i * 0.5;
		double upper_end = lower_end + 0.5;
		test_suite_results << "interval: " << lower_end << "% " << upper_end << "% : " << time_middle_point_method_histogram[i] << endl;
	}

	test_suite_results << endl << "golden_section_method_histogram" << endl;
	for ( i = 0; i < time_golden_section_method_histogram.size(); i++)
	{
		double lower_end = i * 0.5;
		double upper_end = lower_end + 0.5;
		test_suite_results << "interval: " << lower_end << "% " << upper_end << "% : " << time_golden_section_method_histogram[i] << endl;
	}

	//*/




































	individual_test_results.close();
	test_suite_results.close();
	return 0;
}












